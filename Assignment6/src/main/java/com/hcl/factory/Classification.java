package com.hcl.factory;

	import java.sql.SQLException;
	import java.util.List;
	import com.hcl.model.Movie;

	public interface Classification {

		public List<Movie> movieType() throws SQLException;

	}



