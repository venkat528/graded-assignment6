package com.hcl.factory;


	import java.sql.Connection;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
	import java.util.ArrayList;
	import java.util.List;

	import com.hcl.database.DBConnection;
	import com.hcl.model.Movie;

	public class TopRatedMovies implements Classification {

		Connection conn = DBConnection.getInstance().getConnection(); // Establishing connection

		public TopRatedMovies() {

		}

		@Override
		public List<Movie> movieType() throws SQLException {

			List<Movie> moviesList = new ArrayList<Movie>(); // displaying movies from database

			// To get data
			String data = "select id,title,year,storyLine,imdbRating,classification from moviedataset where Classification ='top rated movies'";
			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery(data);
			while (result.next()) {
				Movie movie = new Movie();
				movie.setId(result.getInt(1));
				movie.setTitle(result.getString(2));
				movie.setYear(result.getInt(3));
				movie.setStoryline(result.getString(4));
				movie.setImdbRating(result.getInt(5));
				movie.setClassification(result.getString(6));
				moviesList.add(movie);
			}

			return moviesList;
		}
	}


