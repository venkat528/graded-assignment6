package com.hcl.database;


	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.SQLException;

	public final class DBConnection {
		private static volatile DBConnection instance;
		private static volatile Connection conn;

		// private constructor
		private DBConnection() {

		}

		public static DBConnection getInstance() {
			// Singleton pattern for connecting database
			if (instance == null) {
				instance = new DBConnection();
			}
			return instance;
		}

		public Connection getConnection() {
			/*
			 * creates new connection if database is not connected else returns existing one
			 */
			if (conn == null) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/movies", "root", "Vijayreddy@143");
					System.out.println("driver loaded");
				} catch (ClassNotFoundException ce) {
					// TODO Auto-generated catch block
					System.err.println("driver not loaded");
					ce.printStackTrace();
				} catch (SQLException se) {
					// TODO Auto-generated catch block
					System.err.println("cannot connect to database");
					se.printStackTrace();
				}
			}
			return conn;
		}

	}



