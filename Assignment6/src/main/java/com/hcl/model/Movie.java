package com.hcl.model;

public class Movie {
	private int id;
	private String title;
	private int year;
	private String storyLine;
	private int imdbRating;
	private String classification;

	public Movie() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getStoryline() {
		return storyLine;
	}

	public void setStoryline(String storyline) {
		this.storyLine = storyline;
	}

	public int getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(int imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	@Override
	public String toString() {
		return "movie [id=" + id + ", title=" + title + ", year=" + year + ", storyline=" + storyLine + ", imdbRating="
				+ imdbRating + ", classification=" + classification + "]";
	}

}



